import flask
from flask_cors import CORS
from flask import jsonify

app = flask.Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

# files stores all file data
files = [{ 'name': 'filename',
		   'id': 1,
		   'raw': None,
		   'extension': 'txt' }]

# directories stores all directory data
directories = [{ 'name': 'root',
			     'id': 1,
			     'contents': [1],
			     'children': [] ,
			     'parent': None },
			   { 'name': 'folder_1',
			     'id': 2,
			     'contents': [1],
			     'children': [], 
			     'parent': 1 }]

# this endpoint will handle showing the directory in its current state
@app.route('/showdirectory', methods=['GET'])
def showdirectory():

	results = build_tree(directories, files)
	return jsonify(results)

# this endpoint will handle adding directories
@app.route('/adddirectory', methods=['PUT'])
def adddirectory():

	results = build_tree(directories, files)
	return jsonify(results)

# this endpoint would handle the removal of directories based off of the ID
@app.route('/removedirectory', methods=['DELETE'])
def removedirectory():

	results = build_tree(directories, files)
	return jsonify(results)


def build_tree(directories, files):
	# reset the tree
	tree = {}

	""" This is how I would build out the directory. 
		I would re-use files / directories and store them easily in a list and map them to the appropriate
		destinations in the tree.  Then return the tree back to the user.
		it (should) make deletion / and adding new files, directories easy and as long as this function
		is created intelligently.  This api should work. """


	# Thjis would take some major ironining out to get right.
	x = 0
	for directory in directories:
		if directory['id'] == 1:
			tree[x] = directory

		if directory['children']:
			tree[x]['children'] = directory['children']
		++x
	return tree


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
